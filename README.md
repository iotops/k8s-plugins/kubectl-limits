# kubectl-limits

Simple plugin to list pods, containers and their requested resources and limits.

## Intallation

Download and place the `kubectl-limits` file in a directory that is in your `PATH` env var.

Make sure it has execution permisions.

## Verification

Check installation with the following command:

```
$ which kubectl-limits
/home/f/bin/kubectl-limits
```

Once verified it can be found in your PATHs.

Check if kubectl can see the plugin.

```
$ kubectl plugin list
The following compatible plugins are available:

/home/f/bin/kubectl-limits
```

## Usage

Simple run `kubectl limits`

```
$ kubectl limits
NAME                                           CONTAINER                           REQ MEM       LIM MEM     REQ CPU    LIM CPU
pod1-a0280ba3f5-t7bmz                          container-name1                     192Mi         256Mi       50m        <none>
pod2-53eeb0baf9-c7t8p                          container-name2                     192Mi         256Mi       50m        <none>
pod3-8470a4f5c3-nkgiv                          container-name3                     256Mi         512Mi       50m        <none>
pod4-43301b682a-x9h9d                          container-name4                     512Mi         768Mi       50m        <none>
```

## ToDo

1. add support for namespaces, currently this assumes you are working on the desired namespace
1. intallation via krew
